/* gmpc-lyrdb (GMPC plugin)
 * Copyright (C) 2008-2009 Qball Cow <qball@sarine.nl>
 * Project homepage: http://gmpcwiki.sarine.nl/
 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>
#include <libmpd/debug_printf.h>
#include <gmpc/plugin.h>
#include <gmpc/gmpc_easy_download.h>
#include <gmpc/metadata.h>
#include <glib.h>
#include <config.h>


gmpcPlugin plugin;

/**
 * Get/Set enabled 
 */
static int lyrdb_get_enabled()
{
	return cfg_get_single_value_as_int_with_default(config, "lyrdb-plugin", "enable", TRUE);
}
static void lyrdb_set_enabled(int enabled)
{
	cfg_set_single_value_as_int(config, "lyrdb-plugin", "enable", enabled);
}

/* Get priority */
static int lyrdb_fetch_cover_priority(void){
	return cfg_get_single_value_as_int_with_default(config, "lyrdb-plugin", "priority", 80);
}
static void lyrdb_fetch_cover_priority_set(int priority){
	cfg_set_single_value_as_int(config, "lyrdb-plugin", "priority", priority);
}

typedef struct Query {
    mpd_Song *song;
    void (*callback)(GList *, gpointer);
    gpointer user_data;
    GList *downloads;
    GList *list;
}Query;

static void lyrdb_download_actual_lyric_callback(const GEADAsyncHandler *handle, GEADStatus status, gpointer data)
{
    Query *q = (Query *)data;
    if(status == GEAD_PROGRESS) return;
    if(status == GEAD_DONE) 
    {
        goffset size = 0;
        const char *data = gmpc_easy_handler_get_data(handle, &size);
        if(g_utf8_validate(data, (int)size,NULL)){
            MetaData *mtd = meta_data_new();
            mtd->type = META_SONG_TXT; 
            mtd->plugin_name = plugin.name;
            mtd->content_type = META_DATA_CONTENT_TEXT;
            mtd->content = g_strdup(data);
            mtd->size = -1;
            q->list = g_list_append(q->list, mtd);
        }
    }
    q->downloads = g_list_remove(q->downloads, handle);
    if(q->downloads == NULL)
    {
        q->callback(q->list, q->user_data);
        g_free(q);
    }
}

static void lyrdb_download_callback(const GEADAsyncHandler *handle, GEADStatus status, gpointer data)
{
    Query *q = (Query *)data;
    int found = FALSE;
    if(status == GEAD_PROGRESS) return;
    if(status == GEAD_DONE) 
    {
        goffset size= 0;
        const char *data = gmpc_easy_handler_get_data(handle, &size);
        /* Check error */
        if(size > 6 && strncmp(data, "error:", 6) == 0 )
        {
            debug_printf(DEBUG_INFO,"Error occured: %s\n", data);
        }
        else if(size > 6)
        {
            /* Parse the file */
            gchar **entries = g_strsplit(data, "\n", -1);
            if(entries)
            {
                int i=0;
                for(i=0;entries[i];i++)
                {

                    char **fields = g_strsplit(entries[i],"\\",-1);
                    if(fields)
                    {
                        /* we need 3 fields, last is always NULL, so this should be safe */
                        if( fields[0] && fields[1] && fields[2])
                        {
                            /**
                             * TODO: Make this check utf8
                             */
                            if(strcasecmp(fields[2], q->song->artist) == 0 && strcasecmp(fields[1], q->song->title) == 0)
                            {
                                gchar *uri_path = NULL;
                                GEADAsyncHandler *handle2;
                                debug_printf(DEBUG_INFO," Goint to download lyric: %s\n", fields[0]);
                                uri_path = g_strdup_printf("http://webservices.lyrdb.com/getlyr.php?q=%s", fields[0]);
                                handle2 = gmpc_easy_async_downloader(uri_path,  lyrdb_download_actual_lyric_callback, q);
                                if(handle2) {
                                    found = TRUE;
                                    q->downloads = g_list_append(q->downloads, handle2);
                                }
                                g_free(uri_path);
                            }
                        }
                        g_strfreev(fields);
                    }
                }
                g_strfreev(entries);
            }

        }
    }
    if(!found)
    {
        q->callback(q->list, q->user_data);
        g_free(q);
    }
}

static void lyrdb_get_uri(mpd_Song *song, MetaDataType type, void (*callback)(GList *list, gpointer data), gpointer user_data)
{
    if(lyrdb_get_enabled() && type == META_SONG_TXT && song && song->artist &&song->title)
    {
        Query *q = g_malloc0(sizeof(*q));
        gchar *artist = gmpc_easy_download_uri_escape(song->artist);
        gchar *title =  gmpc_easy_download_uri_escape(song->title);

        gchar *uri_path = g_strdup_printf("http://webservices.lyrdb.com/lookup.php?q=%s%%20%s&for=fullt&agent=gmpc-lyrdb",
                artist, title);
        q->callback = callback;
        q->song = song;
        q->user_data = user_data;
        q->list = NULL;
        q->downloads = NULL;
        g_free(artist); g_free(title);
        if(gmpc_easy_async_downloader(uri_path, lyrdb_download_callback, q)!= NULL)
        {
            g_free(uri_path);
            return;
        }
        g_free(q);
        g_free(uri_path);
    }
    /* Return nothing found */
    callback(NULL, user_data);
}

/**
 * Preferences
 */
static void preferences_construct(GtkWidget *container)
{
    GtkWidget *label = gtk_label_new("Lyrdb.com Web Services are kindly provided by Flavio González Vázquez");
	GtkWidget *vbox = gtk_vbox_new(FALSE, 6);

	gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE,0);

	gtk_widget_show_all(vbox);
	gtk_container_add(GTK_CONTAINER(container), vbox);
}

/**
 * Generic de-construct function
 */
static void preferences_destroy(GtkWidget *container)
{
	GtkWidget *widget = gtk_bin_get_child(GTK_BIN(container));
	if(widget)
	{
		gtk_container_remove(GTK_CONTAINER(container),widget);
	}
}

gmpcMetaDataPlugin lw_cover = {
	.get_priority   = lyrdb_fetch_cover_priority,
    .set_priority   = lyrdb_fetch_cover_priority_set,
	.get_metadata = lyrdb_get_uri
};

gmpcPrefPlugin lw_pref = {
	.construct = preferences_construct,
	.destroy = preferences_destroy
};

int plugin_api_version = PLUGIN_API_VERSION;

gmpcPlugin plugin = {
	.name           = "Lyrdb.com lyric source",
	.version        = {PLUGIN_MAJOR_VERSION,PLUGIN_MINOR_VERSION,PLUGIN_MICRO_VERSION},
	.plugin_type    = GMPC_PLUGIN_META_DATA,
	.metadata       = &lw_cover,
	.get_enabled    = lyrdb_get_enabled,
	.set_enabled    = lyrdb_set_enabled,
	.pref 		= &lw_pref
};
